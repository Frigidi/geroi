﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_of_Might_and_Magic_III_HD_Try_Remake
{
    class Cavalry : Warriors
    {
        public Cavalry(int x, int y, int squad) : base((char)11, x, y, 5 * squad, 4 * squad, 15, 5) { }

        public void Move()
        {
            Console.SetCursorPosition(1, 21);
            Console.Write("Введите колво клеток для хода: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Выберете сторону, в которую должен идти персонаж: ");
            int step = int.Parse(Console.ReadLine());

            if (n <= 15)
            {
                switch (step)
                {
                    case 1:
                        SetX(GetX() + n); //шаг назад
                        break;

                    case 2:
                        SetX(GetX() - n);//шаг вперед
                        break;

                    case 3:
                        SetY(GetY() - n);//шаг вверх
                        break;

                    case 4:
                        SetY(GetY() + n);//шаг вниз
                        break;
                }
            }
        }
    }
}
