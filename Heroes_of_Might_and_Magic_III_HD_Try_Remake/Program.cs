﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_of_Might_and_Magic_III_HD_Try_Remake
{
    class Program
    {
        #region Поле
        static void Battlefield()
        {
            for (int i = 0; i < 20; i++)
            {
                Console.SetCursorPosition(35, i);
                Console.WriteLine("|");
            }

            for (int i = 0; i < 80; i++)
            {
                Console.SetCursorPosition(i, 20);
                Console.WriteLine("-");
            }
        }
        #endregion

        #region Как ходить?
        static void Options()
        {
            Console.SetCursorPosition(60, 21);
            Console.WriteLine("Что бы походить: ");
            Console.SetCursorPosition(60, 22);
            Console.WriteLine("Направо - 1 ");
            Console.SetCursorPosition(60, 23);
            Console.WriteLine("Налево - 2");
            Console.SetCursorPosition(60, 24);
            Console.WriteLine("Вверх - 3 ");
            Console.SetCursorPosition(60, 25);
            Console.WriteLine("Вниз - 4");
        }
        #endregion

        #region Атаковать противника
        static void Optionss()
        {
            Console.SetCursorPosition(60, 21);
            Console.WriteLine("Что бы атаковать: ");
            Console.SetCursorPosition(60, 22);
            Console.WriteLine("Лучника - 1 ");
            Console.SetCursorPosition(60, 23);
            Console.WriteLine(" Мага  -  2 ");
            Console.SetCursorPosition(60, 24);
            Console.WriteLine("Пехоту - 3 ");
            Console.SetCursorPosition(60, 25);
            Console.WriteLine("Конницу - 4");
        }
        #endregion

        #region Ввод координат и кол-ва отрядов
        static void Position(ref int x, ref int y, ref int squad)
        {
            Console.SetCursorPosition(1, 21);
            Console.Write("Введите расположение отряда по горизонтали x: ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите расположение отряда по вердикали y: ");
            y = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите кол-во отрядов: ");
            squad = int.Parse(Console.ReadLine());
        }
        #endregion

        static void Main(string[] args)
        {
            #region Расстановка
            int x = 0, y = 0, squad = 0;

            Console.SetCursorPosition(1, 20);
            Console.WriteLine("Расстановка сил левого игрока");
            Position(ref x, ref y, ref squad);
            Archer ar1 = new Archer(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Лучник расположен на поле. {(char)1}");
            Position(ref x, ref y, ref squad);
            Sorcerer sor1 = new Sorcerer(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Маг расположен на поле. {(char)6}");
            Position(ref x, ref y, ref squad);
            Dogface dog1 = new Dogface(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Пехотинец расположен на поле. {(char)64}");
            Position(ref x, ref y, ref squad);
            Cavalry cav1 = new Cavalry(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Конница расположена на поле. {(char)11}");

            Console.SetCursorPosition(1, 20);
            Console.WriteLine("Расстановка сил правого игрока");
            Position(ref x, ref y, ref squad);
            Archer ar2 = new Archer(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Лучник расположен на поле. {(char)1}");
            Position(ref x, ref y, ref squad);
            Sorcerer sor2 = new Sorcerer(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Маг расположен на поле. {(char)6}");
            Position(ref x, ref y, ref squad);
            Dogface dog2 = new Dogface(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Пехотинец расположен на поле. {(char)64}");
            Position(ref x, ref y, ref squad);
            Cavalry cav2 = new Cavalry(x, y, squad);
            Console.Clear();
            Console.WriteLine($"Конница расположена на поле. {(char)11}");
            #endregion

            bool game = true;

            while (game)
            {
                Console.Clear();

                Battlefield();
                Options();

                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }

                #region Левый игрок

                #region Лучник левого
                if (ar1.GetHP() > 0)
                {
                    ar1.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход левого лучника(он может ходить на 2 клетки и бить по всей карте)");
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select = int.Parse(Console.ReadLine());
                    switch (select)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (ar1.AttackRange(ar2)) { ar1.AttackBush(ar2); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar1.Move(); }
                                    break;
                                case 2:
                                    if (ar1.AttackRange(sor2)) { ar1.AttackBush(sor2); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar1.Move(); }
                                    break;
                                case 3:
                                    if (ar1.AttackRange(dog2)) { ar1.AttackBush(dog2); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar1.Move(); }
                                    break;
                                case 4:
                                    if (ar1.AttackRange(cav2)) { ar1.AttackBush(cav2); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar1.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            ar1.Move();
                            break;
                    }
                }
                #endregion

                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion

                #region Маг левого
                if (sor1.GetHP() > 0)
                {
                    sor1.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход левого мага(он может ходить на 2 клетки и бить по всей карте)");                    
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select1 = int.Parse(Console.ReadLine());
                    switch (select1)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (sor1.AttackRange(ar2)) { sor1.AttackBush(ar2); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor1.Move(); }
                                    break;
                                case 2:
                                    if (sor1.AttackRange(sor2)) { sor1.AttackBush(sor2); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor1.Move(); }
                                    break;
                                case 3:
                                    if (sor1.AttackRange(dog2)) { sor1.AttackBush(dog2); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor1.Move(); }
                                    break;
                                case 4:
                                    if (sor1.AttackRange(cav2)) { sor1.AttackBush(cav2); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor1.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            sor1.Move();
                            break;
                    }
                }
                #endregion

                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion

                #region Пехота левого
                if (dog1.GetHP() > 0)
                {
                    dog1.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход левого пехотинца(он может ходить на 8 клеток и бить на растояние 3 клеток)");
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select2 = int.Parse(Console.ReadLine());
                    switch (select2)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (dog1.AttackRange(ar2)) { dog1.AttackBush(ar2); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog1.Move(); }
                                    break;
                                case 2:
                                    if (dog1.AttackRange(sor2)) { dog1.AttackBush(sor2); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog1.Move(); }
                                    break;
                                case 3:
                                    if (dog1.AttackRange(dog2)) { dog1.AttackBush(dog2); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog1.Move(); }
                                    break;
                                case 4:
                                    if (dog1.AttackRange(cav2)) { dog1.AttackBush(cav2); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog1.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            dog1.Move();
                            break;
                    }
                }
                #endregion

                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion

                #region Конница левого
                if (cav1.GetHP() > 0)
                {
                    cav1.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход левой конницы(она может ходить на 15 клеток и бить на растояние 5 клеток)");
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select3 = int.Parse(Console.ReadLine());
                    switch (select3)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (cav1.AttackRange(ar2)) { cav1.AttackBush(ar2); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav1.Move(); }
                                    break;
                                case 2:
                                    if (cav1.AttackRange(sor2)) { cav1.AttackBush(sor2); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav1.Move(); }
                                    break;
                                case 3:
                                    if (cav1.AttackRange(dog2)) { cav1.AttackBush(dog2); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav1.Move(); }
                                    break;
                                case 4:
                                    if (cav1.AttackRange(cav2)) { cav1.AttackBush(cav2); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav1.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            cav1.Move();
                            break;
                    }
                }
                #endregion

                #endregion
                
                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion


                #region Правый игрок

                #region Лучник правого
                if (ar2.GetHP() > 0)
                {
                    ar2.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход правого лучника(он может ходить на 2 клетки и бить по всей карте)");
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select4 = int.Parse(Console.ReadLine());
                    switch (select4)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (ar2.AttackRange(ar2)) { ar2.AttackBush(ar1); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar2.Move(); }
                                    break;
                                case 2:
                                    if (ar2.AttackRange(sor1)) { ar2.AttackBush(sor1); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar2.Move(); }
                                    break;
                                case 3:
                                    if (ar2.AttackRange(dog1)) { ar2.AttackBush(dog1); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar2.Move(); }
                                    break;
                                case 4:
                                    if (ar2.AttackRange(cav1)) { ar2.AttackBush(cav1); }
                                    else { Console.WriteLine("Противник слишком далеко"); ar2.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            ar2.Move();
                            break;
                    }
                }
                #endregion

                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion

                #region Маг правого
                if (sor2.GetHP() > 0)
                {
                    sor2.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход правого мага(он может ходить на 2 клетки и бить по всей карте)");
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select5 = int.Parse(Console.ReadLine());
                    switch (select5)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (sor2.AttackRange(ar2)) { sor2.AttackBush(ar1); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor2.Move(); }
                                    break;
                                case 2:
                                    if (sor2.AttackRange(sor1)) { sor2.AttackBush(sor1); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor2.Move(); }
                                    break;
                                case 3:
                                    if (sor2.AttackRange(dog1)) { sor2.AttackBush(dog1); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor2.Move(); }
                                    break;
                                case 4:
                                    if (sor2.AttackRange(cav1)) { sor2.AttackBush(cav1); }
                                    else { Console.WriteLine("Противник слишком далеко"); sor2.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            sor2.Move();
                            break;
                    }
                }
                #endregion

                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion

                #region Пехота правого
                if (dog2.GetHP() > 0)
                {
                    dog2.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход правого пехотинца(он может ходить на 8 клеток и бить на растояние 3 клеток)");
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select6 = int.Parse(Console.ReadLine());
                    switch (select6)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (dog2.AttackRange(ar2)) { dog2.AttackBush(ar1); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog2.Move(); }
                                    break;
                                case 2:
                                    if (dog2.AttackRange(sor1)) { dog2.AttackBush(sor1); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog2.Move(); }
                                    break;
                                case 3:
                                    if (dog2.AttackRange(dog1)) { dog2.AttackBush(dog1); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog2.Move(); }
                                    break;
                                case 4:
                                    if (dog2.AttackRange(cav1)) { dog2.AttackBush(cav1); }
                                    else { Console.WriteLine("Противник слишком далеко"); dog2.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            dog2.Move();
                            break;
                    }
                }
                #endregion

                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion

                #region Конница правого
                if (cav2.GetHP() > 0)
                {
                    cav2.Show();
                    Console.SetCursorPosition(1, 26);
                    Console.WriteLine("Ход левой конницы(она может ходить на 15 клеток и бить на растояние 5 клеток)");
                    Console.SetCursorPosition(1, 23);
                    Console.Write("Выберете, атаковать - 1, ходить - 2:");
                    int select7 = int.Parse(Console.ReadLine());
                    switch (select7)
                    {
                        case 1:
                            Optionss();
                            Console.SetCursorPosition(1, 24);
                            Console.Write("Выберете, кого атаковать:");
                            int ataka = int.Parse(Console.ReadLine());
                            switch (ataka)
                            {
                                case 1:
                                    if (cav2.AttackRange(ar2)) { cav2.AttackBush(ar1); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav2.Move(); }
                                    break;
                                case 2:
                                    if (cav2.AttackRange(sor1)) { cav2.AttackBush(sor1); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav2.Move(); }
                                    break;
                                case 3:
                                    if (cav2.AttackRange(dog1)) { cav2.AttackBush(dog1); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav2.Move(); }
                                    break;
                                case 4:
                                    if (cav2.AttackRange(cav1)) { cav2.AttackBush(cav1); }
                                    else { Console.WriteLine("Противник слишком далеко"); cav2.Move(); }
                                    break;
                            }
                            break;

                        case 2:
                            cav2.Move();
                            break;
                    }


                }
                #endregion

                #endregion

                #region Очищение
                Console.Clear();
                if (ar1.GetHP() > 0) { ar1.Show(); }
                if (sor1.GetHP() > 0) { sor1.Show(); }
                if (dog1.GetHP() > 0) { dog1.Show(); }
                if (cav1.GetHP() > 0) { cav1.Show(); }
                if (ar2.GetHP() > 0) { ar2.Show(); }
                if (sor2.GetHP() > 0) { sor2.Show(); }
                if (dog2.GetHP() > 0) { dog2.Show(); }
                if (cav2.GetHP() > 0) { cav2.Show(); }
                Battlefield();
                Options();
                #endregion

                if (ar1.GetHP() <= 0 && sor1.GetHP() <= 0 && dog1.GetHP() <= 0 && cav1.GetHP() <= 0)
                {
                    game = false;
                    Console.WriteLine("Победил правый игрок!");
                }
                else if (ar2.GetHP() <= 0 && sor2.GetHP() <= 0 && dog2.GetHP() <= 0 && cav2.GetHP() <= 0)
                {
                    game = false;
                    Console.WriteLine("Победил левый игрок!");
                }



                Console.ReadKey();

            }
        }
    }
}

