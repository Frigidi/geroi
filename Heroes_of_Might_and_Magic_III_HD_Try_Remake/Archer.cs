﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_of_Might_and_Magic_III_HD_Try_Remake
{
    class Archer:Warriors
    {
        public Archer(int x, int y, int squad) : base((char)1, x, y, 2 * squad, 2 * squad, 2, 70) { }
        
        public void Move()
        {
            Console.SetCursorPosition(1, 21);
            Console.Write("Введите колво клеток для хода: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Выберете сторону, в которую должен идти персонаж: ");
            int step = int.Parse(Console.ReadLine());

            if (n <= 2)
            {
                switch (step)
                {
                    case 1:
                        SetX(GetX() + n); //шаг назад
                        break;

                    case 2:
                        SetX(GetX() - n);//шаг вперед
                        break;

                    case 3:
                        SetY(GetY() - n);//шаг вверх
                        break;

                    case 4:
                        SetY(GetY() + n);//шаг вниз
                        break;
                }
            }           
        }
    }
}
