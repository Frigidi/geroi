﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_of_Might_and_Magic_III_HD_Try_Remake
{
    abstract class Warriors
    {
        private int _MAX_X = 70;
        private int _MAX_Y = 20;
        private int _MIN_X = 0;
        private int _MIN_Y = 2;

        private int _attack;
        private int _hp;
        private char _look;
        private int _x, _y;
        private int _go;
        private int _range;

        public Warriors(char look, int x, int y, int attack, int hp, int go, int range)
        {
            _attack = attack;
            _hp = hp;
            _look = look;
            _x = x;
            _y = y;
            _go = go;
            _range = range;
        }

        public void Show()
        {
            Console.SetCursorPosition(_x, _y);
            Console.Write(_look);
        }

        protected void SetX(int x)
        {
            if (x >= _MIN_X && x <= _MAX_X)
            {
                _x = x;
            }
        }

        protected void SetY(int y)
        {
            if (y >= _MIN_Y && y <= _MAX_Y)
            {
                _y = y;
            }
        }

        protected int GetX() { return _x; }
        protected int GetY() { return _y; }

        public int GetHP() { return _hp; }

        public bool AttackRange(Warriors user)
        {
            //return _x - user._x <= n && _x - user._x >= -n && _y - user._y <= n && _y - user._y >= -n;
            return Math.Abs(_x - user._x) + Math.Abs(_y - user._y) <= (_range + _go);
        }

        public void AttackBush(Warriors user)
        {
            if (AttackRange(user) == true)
            {
                user._hp -= _attack;
            }
        }
    }
}
